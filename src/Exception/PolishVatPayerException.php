<?php

namespace malek83\PolishVatPayer\Exception;

/**
 * Abstract excepton class for PolishVatPayer library
 *
 * @package malek83\PolishVatPayer\Exception
 */
abstract class PolishVatPayerException extends \Exception
{
}